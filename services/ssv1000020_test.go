//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"testing"
)

var AC050001 = `{
    "Form": [
        {
            "FormHead": {
                "FormID": ""
            },
            "FormData": {
				"TotalFlowNum":1

            }
        }
    ]
}`

func (this *Ssv1000020Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	switch serviceKey {
	case "AC050001":
		responseData = []byte(AC050001)
	}

	return responseData, nil
}

func TestSsv1000016Impl_Ssv1000020(t *testing.T) {

	Ssv1000020Impl := new(Ssv1000020Impl)

	_, _ = Ssv1000020Impl.Ssv1000020(&models.SSV1000020I{
		TxnAccountingFlows: []models.TxnAccountingFlows{
			{
				TxnDate:        "1",
				GlobalBizSeqNo: "1",
				TranBrno:       "1",
				TranChannel:    "1",
				BussSys:        "1",
				TranReqCode:    "1",
				HandleType:     "1",
				HandleTypeSeq:  0,
				AcctDiff:       "1",
				AcctingOrgId:   "1",
				AcctNo:         "1",
				ContNo:         "1",
				Cur:            "1",
				Amt:            0,
				QuivalenceAmt:  0,
				ExchRate:       0,
				TranSerialNo:   "1",
				SrcTranDate:    "1",
			}},
	})

}
