//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000020/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
)

type Ssv1000020Impl struct {
	services.CommonService
	//TODO ADD Service Self Define Field
	Ssv100020O *models.SSV1000020O
	Ssv100020I *models.SSV1000020I
	Ac050001O  *models.AC050001O
}

// @Desc Ssv1000020 process
// @Author
// @Date 2020-12-12
func (impl *Ssv1000020Impl) Ssv1000020(ssv1000020I *models.SSV1000020I) (ssv1000020O *models.SSV1000020O, err error) {

	impl.Ssv100020I = ssv1000020I

	//调用核算流水登记
	err = impl.RegAcctFlow()
	if err != nil {
		return nil, errors.New("Failed to register accounting flow", constant.ERRCODE01)
	}

	ssv1000020O = &models.SSV1000020O{
		TotalFlowNum: impl.Ac050001O.TotalFlowNum,
	}

	return ssv1000020O, nil
}

//调用登记核算流水
func (impl *Ssv1000020Impl) RegAcctFlow() error {
	ac050001I := models.AC050001I{
		TxnAccountingFlows: impl.Ssv100020I.TxnAccountingFlows,
	}

	//pack request
	res := &models.AC050001RequestForm{}
	reqBody, err := res.PackRequest(ac050001I)
	if err != nil {
		return err
	}

	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_TYPE_ELEMENT_ID, constant.AC050001, reqBody)
	if err != nil {
		return err
	}

	ac050001Res := &models.AC050001ResponseForm{}
	ac050001O, err := ac050001Res.UnPackResponse(resBody)
	if err != nil {
		return err
	}

	impl.Ac050001O = &ac050001O
	return nil
}
