//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000020/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000020Controller struct {
	controllers.CommController
}

func (*Ssv1000020Controller) ControllerName() string {
	return "Ssv1000020Controller"
}

// @Desc ssv1000020 controller
// @Author
// @Date 2020-12-12
func (c *Ssv1000020Controller) Ssv1000020() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000020Controller.Ssv1000020 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000020I := &models.SSV1000020I{}
	if err := models.UnPackRequest(c.Req.Body, ssv1000020I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv1000020I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000020 := &services.Ssv1000020Impl{}
	ssv1000020.New(c.CommController)
	ssv1000020.Ssv100020I = ssv1000020I

	ssv1000020O, err := ssv1000020.Ssv1000020(ssv1000020I)

	if err != nil {
		log.Errorf("Ssv1000020Controller.Ssv1000020 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv1000020O.PackResponse()
	if err != nil {
		c.SetServiceError(err)
		return
	}
	c.SetAppBody(responseBody)
}

// @Title Ssv1000020 Controller
// @Description ssv1000020 controller
// @Param Ssv1000020 body models.SSV1000020I true body for SSV1000020 content
// @Success 200 {object} models.SSV1000020O
// @router /create [post]
func (c *Ssv1000020Controller) SWSsv1000020() {
	//Here is to generate API documentation, no need to implement methods
}
