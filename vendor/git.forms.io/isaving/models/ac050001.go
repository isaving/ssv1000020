package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC050001I struct {
	TxnAccountingFlows []TxnAccountingFlows
}

type AC050001O struct {
	TotalFlowNum int `json:"TotalFlowNum"`
}

type AC050001IDataForm struct {
	FormHead CommonFormHead
	FormData AC050001I
}

type AC050001ODataForm struct {
	FormHead CommonFormHead
	FormData AC050001O
}

type AC050001RequestForm struct {
	Form []AC050001IDataForm
}

type AC050001ResponseForm struct {
	Form []AC050001ODataForm
}

// @Desc Build request message
func (o *AC050001RequestForm) PackRequest(AC050001I AC050001I) (responseBody []byte, err error) {

	responseForm := AC050001RequestForm{
		Form: []AC050001IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC050001I001",
				},
				FormData: AC050001I,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC050001RequestForm) UnPackRequest(request []byte) (AC050001I, error) {
	AC050001I := AC050001I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC050001I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC050001I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC050001ResponseForm) PackResponse(AC050001O AC050001O) (responseBody []byte, err error) {
	responseForm := AC050001ResponseForm{
		Form: []AC050001ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC050001IO01",
				},
				FormData: AC050001O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC050001ResponseForm) UnPackResponse(request []byte) (AC050001O, error) {

	AC050001O := AC050001O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC050001O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC050001O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (o *AC050001I) validate() error {
	validate := validator.New()
	return validate.Struct(o)

}
