package models

type AmtField struct {
	Amt string `xml:",chardata"`
	Ccy string `xml:"Ccy,attr"`
}
