//Version: v0.01
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV0000003I struct {
	QryTpy        string `valid:"Required;MaxSize(30)"` //查询类型
	MediaType     string `valid:"MaxSize(4)"`           //介质类型
	MediaNm       string `valid:"MaxSize(40)"`          //介质号码
	AgreementId   string `valid:"MaxSize(30)"`          //合约号
	AgreementType string `valid:"MaxSize(5)"`           //合约类型
	CstmrId       string `valid:"Required;MaxSize(20)"` //客户编号
}

type SSV0000003O struct {
	Records  []ContInfo //合约信息

}

type ContInfo struct {
	AgreementType   string  //合约类型
	AgreementId     string  //合约号
	Currency        string  //币种
	CashtranFlag    string  //钞汇标识
	Balance         float64 //余额
	FrzAmount       float64 //冻结金额
	AvlBalance      float64 //可用余额
	AccStatus       string  //账户状态
	AccOpnDt        string  //开户日期
	CstmrCntctPh    string  //手机号
	CstmrCntctAdd   string  //联系地址
	CstmrCntctEm    string  //邮箱
	AccuntNme       string  //账户姓名
}

// @Desc Build request message
func (o *SSV0000003I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV0000003I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV0000003O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV0000003O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV0000003I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV0000003I) GetServiceKey() string {
	return "ssv0000003"
}
