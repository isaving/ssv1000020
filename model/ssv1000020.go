package model

type SSV1000020I struct {
	TxnAccountingFlows []TxnAccountingFlows `json:"TxnAccountingFlows"`
}

type TxnAccountingFlows struct {
	TxnDate        string  `validate:"required" json:"TxnDate"`        //交易日期
	GlobalBizSeqNo string  `validate:"required" json:"GlobalBizSeqNo"` //全局业务流水号
	TranBrno       string  `validate:"required" json:"TranBrno"`       //交易机构
	TranChannel    string  `validate:"required" json:"TranChannel"`    //交易发起渠道
	BussSys        string  `validate:"required" json:"BussSys"`        //业务系统
	TranReqCode    string  `validate:"required" json:"TranReqCode"`    //交易请求码
	HandleType     string  `validate:"required" json:"HandleType"`     //处理类型
	HandleTypeSeq  int64   `validate:"required" json:"HandleTypeSeq"`  //处理类型序号
	AcctDiff       string  `validate:"required" json:"AcctDiff"`       //本方账户区分
	AcctingOrgId   string  `validate:"required" json:"AcctingOrgId"`   //本方账务行所
	AcctNo         string  `json:"AcctNo"`                             //本方客户账号
	ContNo         string  `json:"ContNo"`                             //本方合约号
	Cur            string  `json:"Cur"`                                //本方币别
	Amt            float64 `json:"Amt"`                                //本方发生额
	QuivalenceAmt  float64 `json:"QuivalenceAmt"`                      //本方等值发生额
	ExchRate       float64 `json:"ExchRate"`                           //本方汇率
	TranSerialNo   string  `json:"TranSerialNo"`                       //原交易流水号
	SrcTranDate    string  `json:"SrcTranDate"`                        //原交易日期
}

type SSV1000020O struct {
	TotalFlowNum int `json:"TotalFlowNum"`
}
